const request = require('request-promise');
const geoLib = require('geolib');
const Promise = require('bluebird');
const config = require('../config');

function nameComparison(a, b) {
    const x = a.name.toLowerCase();
    const y = b.name.toLowerCase();
    return x < y ? -1 : x > y ? 1 : 0;
}

function addressFromPartner(partner) {
    return partner.address.no + " " + partner.address.street + " " + partner.address.city + " " + partner.address.country;
}

function isWithinDistance(geoFromHere) {
    let distance = geoLib.getDistance(
        {latitude: config.webbioLatitude, longitude: config.webbioLongitude},
        {latitude: geoFromHere.Latitude, longitude: geoFromHere.Longitude});
    return distance < config.maxDistanceInMeters;
}

function getPartnersWithinRange(partners) {
    return Promise.mapSeries(partners, function (partner) {
        return request(config.baseUrl + addressFromPartner(partner))
            .then(function (body) {
                try {
                    return (JSON.parse(body)["Response"]["View"][0]["Result"][0]["Location"]["DisplayPosition"]);
                } catch {
                    console.log("API could not find location: " + partner.name)
                }
            });
    }).then(function (results) {
        return partners
            .sort(nameComparison)
            .filter((partner, i) => {
                if (results[i] !== undefined) {
                    if (isWithinDistance(results[i])) {
                        return partner;
                    }
                }
            });
    });
}

module.exports = {nameComparison, addressFromPartner, isWithinDistance, getPartnersWithinRange};
