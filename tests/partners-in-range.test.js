const helper = require('../src/partners-in-range');

test('sorts array of object by parameter "name"', () => {
    let strings = [{name:"a"}, {name:"c"}, {name:"d"}, {name:"b"}, {name:"e"}];
    expect(strings.sort(helper.nameComparison)).toEqual([{name:"a"}, {name:"b"}, {name:"c"}, {name:"d"}, {name:"e"}]);
});

test('sorts array of object by parameter "name" 2', () => {
    let strings = [{name:"b"}, {name:"a"}, {name:"d"}, {name:"c"}, {name:"e"}];
    expect(strings.sort(helper.nameComparison)).toEqual([{name:"a"}, {name:"b"}, {name:"c"}, {name:"d"}, {name:"e"}]);
});

test('Coordinates are not within distance to the main office', () => {
    expect(helper.isWithinDistance({Latitude: 10, Longitude: 20})).toBe(false);
});

test('Coordinates are within distace to the main office', () => {
    expect(helper.isWithinDistance({Latitude: 51.8, Longitude: 5.1})).toBe(true);
});

test('Coordinates are within distace to the main office', () => {
   return helper.getPartnersWithinRange(require('../resources/partners')).then(function(result){
       expect(result).toBeDefined();
       expect(result.length).toBe(8);
   });
});

test('Expect specific address from partner', () => {
    expect(helper.addressFromPartner(require('../resources/partners')[0])).toEqual("185 Hagmolenbeekweg Enschede The Netherlands")
});
